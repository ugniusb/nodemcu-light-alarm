------------------------------------------------------------------------------
-- Alarm-related functions
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local log = (require 'log').log
local alarms = {}


local API_TEMPLATE = 'http://192.168.117.200:5000/plain/v0.1.0/alarm/%s/next'
local ALARM_UUID = 'ebb7ccc2-5f2d-478c-a2fa-c19ee899344a'
local ALARM_FILE = 'alarms.txt'


function alarms.fetch_next_alarms(num_attempts, delay, on_success, on_failure)
    local API_CALL_URL = API_TEMPLATE:format(ALARM_UUID)
    local attempts = 0
    local function go()
        http.get(API_CALL_URL, nil, function(code, data)
            log('Alrm :: got HTTP ' .. code)
            if code < 0 then
                attempts = attempts + 1
                if attempts < num_attempts then
                    log('        retrying...')
                    local call_later = (require 'delay').call_later
                    call_later(delay, go)
                else
                    log('        attempts ran out')
                    on_failure()
                end
            elseif code >= 200 and code < 300 then
                local utils = require 'utils'
                log('        success')
                on_success(utils.parse_ints(data))
            else  -- other codes that are not OK
                log('        failure')
                on_failure()
            end
        end)
    end
    go()
end


function alarms.update_alarms_on_file(alarms)
    local time_now = (require 'time').now
    local utils = require 'utils'
    table.sort(alarms)
    now = time_now()
    utils.with_file(ALARM_FILE, 'w', function(f)
        for _, a in ipairs(alarms) do
            if a > now then
                f:writeline(tostring(a))
            end
        end
    end)
end


function alarms.get_next_alarm()
    local utils = require 'utils'
    return utils.with_file(ALARM_FILE, 'r', function(f)
        local time_now = (require 'time').now
        log('Alrm :: reading from file')
        if f == nil then
            log('        no file found')
            return nil
        end
        now = time_now()
        l = f:readline()
        while l ~= nil do
            log('        got line ' .. l)
            t = tonumber(l)
            if t ~= nil and t > now then
                log('        got alarm ' .. t)
                return t
            end
            l = f:readline()
        end
        log('        no alarms found')
        return nil  -- no alarms left
    end)
end


return alarms
