------------------------------------------------------------------------------
-- Wrappers for time-related functions
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local call_later = (require 'delay').call_later
local log = (require 'log').log
local with_file = (require 'utils').with_file
local time = {}

local LAST_UPDATED_FILE = 'time_last_updated.txt'


function time.update_ntp(num_attempts, delay, on_success, on_failure)
    local attempts = 0
    local function go()
        sntp.sync(nil,
            function(sec, usec, server, info)
                log('Time :: got NTP ' .. sec)
                set_last_updated(sec)
                on_success()
            end,
            function(err_type, info)
                attempts = attempts + 1
                if attempts < num_attempts then
                    log('Time :: retrying...')
                    call_later(delat, go)
                else
                    log('Time :: attempts ran out')
                    on_failure()
                end
            end)
    end
    go()
end


function set_last_updated(t)
    with_file(LAST_UPDATED_FILE, 'w', function(f)
        f:writeline(tostring(t))
    end)
end


function time.last_updated()
    return with_file(LAST_UPDATED_FILE, 'r', function(f)
        if f ~= nil then
            return nil
        end
        return tonumber(f:readline())
    end)
end


function time.now()
    local sec, usec, rate = rtctime.get()
    return sec + usec / 1000000
end


-- function time.sleep_until(end_)
--     local MIN_SLEEP_MICROS = 50000  -- see node.sleep()
--     local MAX_SLEEP_MICROS = 268435454
--     log('Time :: sleep_until :: wake at ' .. end_ .. ' s')
--     if time.now() < end_ then
--         local remaining = (end_ - time.now()) * 1000000  -- s -> us
--         log('Time :: sleep_until :: left ' .. remaining .. ' us')
--         if remaining > MAX_SLEEP_MICROS then
--             remaining = MAX_SLEEP_MICROS
--         elseif remaining < MIN_SLEEP_MICROS then
--             return
--         end
--         log('Time :: sleep_until :: sleeping for ' .. remaining .. ' us...')
--         node.sleep({duration = remaining, resume_cb = function()
--             time.sleep_until(time)
--         end})
--     end
-- end
--
--
-- function time.sleep(seconds)
--     local end_ = time.now() + seconds
--     log('Time :: sleep :: for ' .. seconds .. ' s')
--     time.sleep_until(end_)
-- end


return time



