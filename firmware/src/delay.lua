------------------------------------------------------------------------------
-- Simple wrappers for some tmr functions
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local delay = {}
local pack = (require 'utils').pack

local MIN_DELAY = 1  -- see tmr docs
local MAX_DELAY = 6870947

function delay.call_later(delay, fn, ...)
    delay = math.max(MIN_DELAY, math.min(MAX_DELAY, delay))
    local args = pack(...)
    return tmr.create():alarm(delay, tmr.ALARM_SINGLE, function()
        fn(unpack(args))
    end)
end


function delay.call_soon(fn, ...)
    local args = pack(...)
    return tmr.create():alarm(MIN_DELAY, tmr.ALARM_SINGLE, function()
        fn(unpack(args))
    end)
end


return delay
