------------------------------------------------------------------------------
-- Light-related functions
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local call_later = (require 'delay').call_later
local log = (require 'log').log
local time_now = (require 'time').now
local try_call = (require 'utils').try_call
local lights = {}


local LIGHT_PIN = 2
local PWM_FREQUENCY = 1000
local PWM_MAX_DUTY = 1023


pwm.setup(LIGHT_PIN, PWM_FREQUENCY, 0)
pwm.stop(LIGHT_PIN)

local light_mode = nil


function lights.stop(cont)
    light_mode = nil
    pwm.stop(LIGHT_PIN)
    pwm.setduty(LIGHT_PIN, 0)
    try_call(cont)
end


function lights.fade_in(end_at, cont)
    light_mode = lights.fade_in
    log('Lght :: fade_in :: starting')
    local MIN_DTIME = 100  -- ms
    pwm.start(LIGHT_PIN)
    local function go()
        if light_mode ~= lights.fade_in then
            log('Lght :: fade_in :: mode changed')
            try_call(cont)
            return
        end
        time_left = (end_at - time_now()) * 1000  -- ms
        if time_left <= 0 then
            log('Lght :: fade_in :: out of time')
            pwm.setduty(LIGHT_PIN, PWM_MAX_DUTY)
            try_call(cont)
        else
            duty = pwm.getduty(LIGHT_PIN)
            duty_left = PWM_MAX_DUTY - duty
            if duty_left <= 0 then
                try_call(cont)
                return
            end
            dduty = 1
            dtime = time_left / duty_left  -- ms
            if dtime < MIN_DTIME then
                dtime = MIN_DTIME
                dduty = math.ceil(duty_left * MIN_DTIME / time_left)
            end
            pwm.setduty(LIGHT_PIN, math.min(duty + dduty, PWM_MAX_DUTY))
            call_later(math.floor(dtime), go)
        end
    end
    go()
end


function lights.random_blink(min_period_ms, max_period_ms, cont)
    log('Lght :: random_blink :: starting')
    light_mode = lights.random_blink
    local E = tonumber('2.7182818')  -- can't have compile-time float consts
    local function off(duration, cont)
        pwm.setduty(LIGHT_PIN, 0)
        call_later(duration, cont)
    end
    local function on()
        if light_mode ~= lights.random_blink then
            log('Lght :: random_blink :: mode changed')
            try_call(cont)
            return
        end
        -- hyperpolic PDF => exponential sampling
        -- if I had log, the correct would be `exp(x*ln(mx) + (1-x)*ln(mi))`
        -- but this is pretty close (rather min biased)
        local x = math.pow((math.pow(node.random(), E) - 1) / (E - 1), 2)
        local z = min_period_ms + (max_period_ms - min_period_ms) * x
        --
        local half_period = z / 2
        pwm.setduty(LIGHT_PIN, PWM_MAX_DUTY)
        call_later(half_period, off, half_period, on)
    end
    on()
    pwm.start(LIGHT_PIN)
end


function lights.pwm(duty, cont)
    light_mode = lights.pwm
    pwm.setduty(LIGHT_PIN, duty)
    try_call(cont)
end


function lights.morse(durs, loop)
    light_mode = lights.morse
    local D = 250
    local function off(durs, idx, cont)
        log('Lght :: morse :: off')
        if light_mode ~= lights.morse then
            log('                        mode changed')
            try_call(cont)
            return
        end
        pwm.setduty(LIGHT_PIN, 0)
        call_later(D, cont, durs, idx)
    end
    local function on(durs, idx)
        if light_mode ~= lights.morse then
            log('Lght :: morse :: on :: mode changed')
            try_call(cont)
            return
        end
        log('Lght :: morse :: on ' .. idx .. '/' .. #durs)
        if idx > #durs then  -- finished
            if loop then
                log('                       looping...')
                call_later(4*D, on, durs, 1)
            else
                log('                       done')
                try_call(cont)
            end
            return
        end
        if durs[idx] > 0 then
            log('                       setting max PWM')
            pwm.setduty(LIGHT_PIN, PWM_MAX_DUTY)
        end
        log('                       waiting for ' .. durs[idx]*D .. ' ms')
        call_later(durs[idx]*D, off, durs, idx+1, on)
    end
    on(durs, 1)
end

return lights
