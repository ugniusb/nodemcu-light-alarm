------------------------------------------------------------------------------
-- Various utilities
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local log = (require 'log').log
local utils = {}


function utils.parse_ints(str)
    ints = {}
    for i in str:gmatch('%d+') do
        log('Util :: parse_ints :: got', i)
        table.insert(ints, tonumber(i))
    end
    return ints
end


function utils.with_file(fname, mode, action)
    f = file.open(fname, mode)
    val = action(f)
    if f ~= nil then
        f:close()
    end
    return val
end


function utils.compose(f, g)
    return function(...)
        return f(g(...))
    end
end


function utils.sequence_callbacks(...)
    cbs = utils.pack(...)
    local function go(idx, cont)
        log('Util :: seqcb :: go ' .. idx .. '/' .. cbs.n)
        if idx <= cbs.n then
            log('                 calling cbs:' .. idx)
            cbs[idx](function()
                log('Util :: seqcb :: go cb ' .. idx + 1 .. '/' .. cbs.n)
                go(idx + 1, cont)
            end)
        end
    end
    return function(cont)
        go(1, cont)
    end
end


function utils.pack(...)
    return { n = select('#', ...), ... }
end


function utils.try_call(fn)
    log('Util :: try_call :: ' .. tostring(fn))
    if fn ~= nil then
        log('                    calling')
        fn()
    end
end

return utils
