------------------------------------------------------------------------------
-- Main application code
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local log = (require 'log').log

function main()
    local call_soon = (require 'delay').call_soon
    local lights = require 'lights'
    local sequence_callbacks = (require 'utils').sequence_callbacks
    lights.stop()
    -- u = require 'utils'; u.sequence_callbacks(update_time)(function() p('yay') end)
    -- u = require 'utils'; u.sequence_callbacks(update_time, update_alarms)(function() p('yay') end)
    -- u = require 'utils'; u.sequence_callbacks(update_time, update_alarms, next_or_sleep)(function() p('yay') end)
    sequence_callbacks(
            update_time,
            update_alarms,
            next_or_sleep
    )(function ()
        call_soon(main)
    end)
end


function reboot_on_fail()
    local REBOOT_ON_FAIL_DELAY = 10*60*1000  -- 10 min
    local call_later = (require 'delay').call_later
    call_later(REBOOT_ON_FAIL_DELAY, node.restart)
end


function update_time(cont)
    local time = require 'time'
    local try_call = (require 'utils').try_call
    log('App  :: update_time')
    time.update_ntp(5, 1000,
        function()  -- on_success
            log('App  :: update_time :: NTP success')
            try_call(cont)
        end,
        function()  -- on_failure
            log('App  :: update_time :: NTP fail')
            now = time.now()
            if now == 0 or now - time.last_updated() > 24*60*60 then  -- 24 h
                log('App  :: update_time :: last update too far ago')
                local lights = require 'lights'
                lights.morse({
                    2, 1, 0,        -- N
                    2, 0,           -- T
                    1, 2, 2, 1, 0,  -- P
                }, true)
                reboot_on_fail()
            else
                log('App  :: update_time :: last update good enough')
                try_call(cont)
            end
        end)
end


function update_alarms(cont)
    local alarms = require 'alarms'
    local try_call = (require 'utils').try_call
    alarms.fetch_next_alarms(5, 1,  -- timeout already ~9s
        function(als)  -- on_success
            log('App  :: update_alarms :: got fresh alarms')
            alarms.update_alarms_on_file(als)
            try_call(cont)
        end,
        function()  -- on_failure
            log('App  :: update_alarms :: fresh failed')
            if alarms.get_next_alarm() ~= nil then
                try_call(cont)
            else
                local lights = require 'lights'
                lights.morse({
                    1, 2, 0,        -- A
                    1, 2, 1, 1, 0,  -- L
                    1, 2, 1, 0,     -- R
                    2, 2, 0,        -- M
                }, true)
                reboot_on_fail()
            end
        end)
end


function next_or_sleep(cont)
    local LONG_SLEEP_INTERVAL = 30*60  -- 10 min
    local SHORT_SLEEP_INTERVAL = 1*60  -- 1 min
    local FADE_START_AT = 1*60  -- 1 min
    local BLINK_MIN_PERIOD = 30   -- ms
    local BLINK_MAX_PERIOD = 1000 -- ms
    local alarms = require 'alarms'
    local call_later = (require 'delay').call_later
    local time_now = (require 'time').now
    local try_call = (require 'utils').try_call
    next_alarm = alarms.get_next_alarm()
    now = time_now()
    if next_alarm == nil then
        log('App  :: next_or_sleep :: no alarm found')
        call_later(LONG_SLEEP_INTERVAL*1000, try_call, cont)
    elseif next_alarm - now <= FADE_START_AT then
        log('App  :: next_or_sleep :: alarm soon / in '
                .. (next_alarm - now)/60 .. ' min')
        local lights = require 'lights'
        lights.fade_in(next_alarm, function()
            lights.random_blink(
                    BLINK_MIN_PERIOD,
                    BLINK_MAX_PERIOD,
                    cont)
        end)
    else
        log('App  :: next_or_sleep :: alarm later / in '
                .. (next_alarm - now)/60 .. ' min')
        call_later(SHORT_SLEEP_INTERVAL, next_or_sleep)
    end
end


main()
