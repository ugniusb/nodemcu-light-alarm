------------------------------------------------------------------------------
-- Startup code
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
(require 'wifih').connect()

if file.exists("DEBUG") then
    p = print

    function dt(table)  -- dump a table
        for k, v in pairs(table) do
            print(k, ":", v)
        end
    end
end


if file.exists("NOSTART") then
    print("init :: Found file NOSTART, aborting.")
elseif (function()
        -- D5: pullup
        gpio.mode(5, gpio.INPUT, gpio.PULLUP)
        local no_run = gpio.read(5) == 0
        gpio.mode(5, gpio.INPUT, gpio.FLOAT)
        return no_run
    end)() then
    print("init :: Pins D5 and GND shorted (or D5 low), aborting.")
else
    local app_files = { "application.lc", "application.lua" }
    for _, f in pairs(app_files) do
        if file.exists(f) then
            print("init :: Executing ["..f.."]")
            dofile(f)
            break
        end
    end
end
