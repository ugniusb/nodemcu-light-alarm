------------------------------------------------------------------------------
-- Logging functions
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local log = {}


function log.log(...)
    if file.exists('LOG_DISABLE') then
        return
    end
    if file.exists('LOG_HEAP') then
        print('HEAP FREE: ' .. node.heap())
    end
    if file.exists('LOG_TIME') then
        local s, us, _ = rtctime.get()
        print('TIME: ' .. s + us/1000000)
    end
    print(...)
end


return log
