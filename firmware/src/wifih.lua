------------------------------------------------------------------------------
-- Simple WiFi connection helper
-- LICENSE: GPLv2
-- (c) Ugnius Beinorius 2018
------------------------------------------------------------------------------
local log = (require 'log').log
local wifih = {}


function wifih.connect()
    cfg = wifi.sta.getconfig(true)
    log("WiFi :: Connecting to [".. cfg.ssid .."]...")
    wifi.setmode(wifi.STATION)
    wifi.sta.connect()
    -- use the following to set credentials
    -- wifi.sta.config({ssid='SSID', pwd='password', save=true})

    wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, function(t)
        log("WiFi :: Connected to [".. t.SSID .."].")
        log("        Waiting for IP address...")
    end)
    wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(t)
        log("WiFi :: Got IP: " .. t.IP)
    end)
    wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, function(t)
        if t.reason == wifi.eventmon.reason.ASSOC_LEAVE then -- station left AP
            return
        end
        log("WiFi :: Connection to [" .. t.SSID .. "] lost.")
        for name, r in pairs(wifi.eventmon.reason) do
            if r == t.reason then
                log("       Disconnect reason: " .. r .. " [" .. name .. "].")
                break
            end
        end
    end)
end


return wifih
