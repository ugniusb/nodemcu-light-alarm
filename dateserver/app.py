#!/usr/bin/env python3
import datetime
import dateutil.parser
import psycopg2 as postgres
import psycopg2.extras as postgres_extras
import time
import yaml
from contextlib import contextmanager
from flask import Flask, g, jsonify, request


with open('config.yaml') as cf:
    config = yaml.load(cf)
app = Flask(__name__)


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class InvalidDateFormat(InvalidUsage):
    def __init__(self, *args, **kwargs):
        super().__init__('Provided date format is not supported',
                         *args, **kwargs)


def db():
    if not hasattr(g, 'db_conn'):
        print(f'Connecting to PostgreSQL with {config["postgres"]}...')
        g.db_conn = postgres.connect(**config['postgres'])
        g.db_conn.autocommit = True
        with g.db_conn.cursor() as cur:
            cur.execute(
                    """
                    CREATE TABLE IF NOT EXISTS alarms (
                        id SERIAL PRIMARY KEY,
                        alarm UUID,
                        time TIMESTAMP WITHOUT TIME ZONE,
                        UNIQUE (alarm, time)
                    );
                    """)
    return g.db_conn


@contextmanager
def cursor():
    conn = db()
    cur = conn.cursor()
    yield cur
    cur.close()


def clamp(min_, max_, value):
    return max(min_, min(max_, value))


def parse_datetime(val):
    try:
        return dateutil.parser.parse(str(val))
    except ValueError:
        pass
    try:
        return datetime.datetime.utcfromtimestamp(int(val))
    except ValueError:
        pass
    raise InvalidDateFormat()


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


def get_next_alarms(alarm, count):
    count = clamp(0, 50, count)
    now = datetime.datetime.utcnow()
    with cursor() as cur:
        cur.execute("""
                    SELECT id, time
                    FROM alarms
                    WHERE alarm = %s AND time >= %s
                    ORDER BY time ASC
                    LIMIT %s
                    """,
                    (str(alarm), now, count))
        return cur.fetchall()


@app.route('/plain/v0.1.0/alarm/<uuid:alarm>/next', defaults={'count': 10})
@app.route('/plain/v0.1.0/alarm/<uuid:alarm>/next/<int:count>')
def get_next_alarms_plain(*args, **kwargs):
    vals = get_next_alarms(*args, **kwargs)
    return '\n'.join(
            str(int(time.mktime(t.timetuple())))
            for _, t in vals)


@app.route('/json/v0.1.0/alarm/<uuid:alarm>/next', defaults={'count': 10})
@app.route('/json/v0.1.0/alarm/<uuid:alarm>/next/<int:count>')
def get_next_alarms_json(*args, **kwargs):
    vals = [{'id': id_, 'time': t.isoformat()}
            for id_, t in get_next_alarms(*args, **kwargs)]
    return jsonify(vals)


@app.route('/json/v0.1.0/alarm/<uuid:alarm>', methods=['PUT'])
def add_alarms(alarm):
    dts = [parse_datetime(str(d)) for d in request.get_json()]
    vals = [(str(alarm), dt) for dt in dts]
    with cursor() as cur:
        postgres_extras.execute_values(
                cur,
                """
                INSERT INTO alarms (alarm, time)
                VALUES %s
                ON CONFLICT (alarm, time) DO UPDATE
                    SET time = EXCLUDED.time
                RETURNING id, time
                """,
                vals)
        return jsonify([{'id': id_, 'time': t.isoformat()}
                        for (id_, t) in cur])


@app.route('/json/v0.1.0/alarm/old', methods=['DELETE'])
def delete_old_alarms():
    with cursor() as cur:
        cur.execute("""
                    DELETE FROM alarms
                    WHERE time <= now()
                    """)
    return ''


@app.route('/json/v0.1.0/alarm/delete_events', methods=['POST'])
def delete_events():
    ids = tuple(map(int, request.get_json()))
    #  try:
    with cursor() as cur:
        cur.execute("""
                    DELETE FROM alarms
                    WHERE id IN %s
                    """,
                    (ids,))
    return ''
    #  except:
    #      raise InvalidUsage('Specified event does not exist')


if __name__ == '__main__':
    app.run(**config['flask'])
